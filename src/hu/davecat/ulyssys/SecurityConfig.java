package hu.davecat.ulyssys;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	
	
	private HikariDataSource dataSource;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().
		antMatchers("/secure/**").hasAnyAuthority("applicants","approver").
		antMatchers("/secure/applicants/**").hasAuthority("applicants").
		antMatchers("/secure/approver/**").hasAuthority("approver").
		and().formLogin(). //login configuration
				failureUrl("/Customlogin.xhtml"). 
                loginPage("/Customlogin.xhtml").
                loginProcessingUrl("/appLogin").
                usernameParameter("app_username").
                passwordParameter("app_password").
                defaultSuccessUrl("/secure/Succeed.xhtml").	
		and().logout().    //logout configuration
		logoutUrl("/appLogout"). 
		logoutSuccessUrl("/Customlogin.xhtml");
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		dataSource = new HikariDataSource();
		dataSource.setJdbcUrl("jdbc:mysql://localhost:3306/test?autoReconnect=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC");
		dataSource.setUsername("root");
		dataSource.setPassword("root");
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setMaximumPoolSize(20);
		
		
		
		auth.inMemoryAuthentication().withUser("asd").password("asd").roles("Applicants1");
		auth.jdbcAuthentication().dataSource(dataSource).
		usersByUsernameQuery("select loginname, password,enabled from user where loginname = ?").
		authoritiesByUsernameQuery("select loginname,type from user where loginname = ?");
	}
	
	
}
