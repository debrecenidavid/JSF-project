package hu.davecat.ulyssys.controller;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import hu.davecat.ulyssys.entity.Application;
import hu.davecat.ulyssys.entity.Event;
import hu.davecat.ulyssys.entity.User;
import hu.davecat.ulyssys.service.ApplicationService;
import hu.davecat.ulyssys.service.EventService;
import hu.davecat.ulyssys.service.UserService;

@ManagedBean
@ViewScoped
public class ApplicationController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6902909281196177363L;

	@ManagedProperty("#{applicationService}")
	ApplicationService applicationService;
	@ManagedProperty("#{userService}")
	UserService userService;
	@ManagedProperty("#{eventService}")
	EventService eventService;

	private Application application = new Application();

	private List<Application> approvedApplications;

	private List<Application> notApprovedApplications;

	private List<Application> approvedApplicationsByUserName;

	private List<Application> notApprovedApplicationsByUserName;

	private User loggedInUser;

	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	private String loggedInLoginname = auth.getName();

	@PostConstruct
	public void loadApplications() {
		loggedInUser = userService.findUserByLoginName(loggedInLoginname);
		approvedApplications = applicationService.findApproved();
		notApprovedApplications = applicationService.findNotApproved();
		approvedApplicationsByUserName = applicationService.findApprovedByUserName(loggedInLoginname);
		notApprovedApplicationsByUserName = applicationService.findNotApprovedByUserName(loggedInLoginname);
	}

	public void saveNewApplication() {
		
		boolean succeed=true;
		try {
			application.setUser(loggedInUser);
			application.setApproved(false);
			applicationService.saveNewApplication(application);
			
		} catch (Exception e) {
			succeed=false;
		}

		Event event = new Event();
		event.setHappening("new application");
		event.setUser(loggedInUser);
		event.setSucceed(succeed);
		event.setDate(new Date());
		eventService.newEvent(event);

		application = new Application();
		loadApplications();

	}


	public void editApplicationAmount() {
		
		boolean succeed=true;
		try {
			applicationService.editApplicationAmountById(application.getId(), application.getAmount());
			
		} catch (Exception e) {
			succeed=false;
		}

		Event event = new Event();
		event.setHappening("Application with id:" + application.getId() + " amount value changed");
		event.setUser(loggedInUser);
		event.setSucceed(succeed);
		event.setDate(new Date());
		eventService.newEvent(event);

		application = new Application();
		loadApplications();

	}

	public void approveApplication(int id) {
		
		boolean succeed=true;
		try {
			applicationService.approveApplicationById(id);
			
		} catch (Exception e) {
			succeed=false;
		}

		Event event = new Event();
		event.setHappening("Application with id:" + id + " approved");
		event.setUser(loggedInUser);
		event.setSucceed(succeed);
		event.setDate(new Date());
		eventService.newEvent(event);

		loadApplications();
	}

	public void deleteApplication(int id) {
		
		boolean succeed=true;
		try {
			applicationService.deleteApplicationById(id);
			
		} catch (Exception e) {
			succeed=false;
		}

		Event event = new Event();
		event.setHappening("Application with id:" + id + " rejected");
		event.setUser(loggedInUser);
		event.setSucceed(succeed);
		event.setDate(new Date());
		eventService.newEvent(event);

		loadApplications();
	}

	public void idForEditApplication(int id) {
		application.setId(id);
	}

	public String checkApprove(boolean b) {
		if (b) {
			return "Approved";
		}
		return "Waiting for approve";
	}

	public String checkSucceed(boolean b) {
		if (b) {
			return "Succeed";
		}
		return "Failed";
	}

	// Getters Setters

	public void setEventService(EventService eventService) {
		this.eventService = eventService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public void setApplicationService(ApplicationService applicationService) {
		this.applicationService = applicationService;
	}

	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	public List<Application> getApprovedApplications() {
		return approvedApplications;
	}

	public void setApprovedApplications(List<Application> approvedApplications) {
		this.approvedApplications = approvedApplications;
	}

	public List<Application> getNotApprovedApplications() {
		return notApprovedApplications;
	}

	public void setNotApprovedApplications(List<Application> notApprovedApplications) {
		this.notApprovedApplications = notApprovedApplications;
	}

	public List<Application> getApprovedApplicationsByUserName() {
		return approvedApplicationsByUserName;
	}

	public void setApprovedApplicationsByUserName(List<Application> approvedApplicationsByUserName) {
		this.approvedApplicationsByUserName = approvedApplicationsByUserName;
	}

	public List<Application> getNotApprovedApplicationsByUserName() {
		return notApprovedApplicationsByUserName;
	}

	public void setNotApprovedApplicationsByUserName(List<Application> notApprovedApplicationsByUserName) {
		this.notApprovedApplicationsByUserName = notApprovedApplicationsByUserName;
	}

}
