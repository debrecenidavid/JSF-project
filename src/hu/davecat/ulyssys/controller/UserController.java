package hu.davecat.ulyssys.controller;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import hu.davecat.ulyssys.entity.Type;
import hu.davecat.ulyssys.entity.User;
import hu.davecat.ulyssys.service.UserService;

@ManagedBean
public class UserController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManagedProperty("#{userService}")
	UserService userService;

	private User loggedInUser;


	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	private String loggedInLoginname = auth.getName();

	@PostConstruct
	public void actualUser() {
		loggedInUser = userService.findUserByLoginName(loggedInLoginname);		

	}
		
	public String getUserMyApplicationsPage() {
		if (loggedInUser.getType().equals(Type.applicants)) {
			return "/secure/applicants/MyApplications.xhtml";
		} else {
			return "/secure/approver/MyApplications.xhtml";
		}
	}

	public String getUserApplicationApprovePage() {
		if (loggedInUser.getType().equals(Type.applicants)) {
			return "/secure/applicants/ApplicationApprove.xhtml";
		} else {
			return "/secure/approver/ApplicationApprove.xhtml";
		}
	}
	
	public User getLoggedInUser() {
		return loggedInUser;
	}

	public void setLoggedInUser(User loggedInUser) {
		this.loggedInUser = loggedInUser;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

}
