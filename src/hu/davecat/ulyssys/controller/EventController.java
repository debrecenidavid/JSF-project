package hu.davecat.ulyssys.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import hu.davecat.ulyssys.entity.Event;
import hu.davecat.ulyssys.entity.Type;
import hu.davecat.ulyssys.entity.User;
import hu.davecat.ulyssys.service.EventService;

@ManagedBean
public class EventController implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManagedProperty("#{eventService}")
	private EventService eventService;
	
	private List<Event> events;
	
	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	String name=auth.getName();
	
	@PostConstruct
	public void loadEvents(){
		events=eventService.findAll();
	}
	
	
	
	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public List<Event> getEvents() {
		return events;
	}


	public void setEvents(List<Event> events) {
		this.events = events;
	}


	public void setEventService(EventService eventService) {
		this.eventService = eventService;
	}
}
