package hu.davecat.ulyssys.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import hu.davecat.ulyssys.entity.User;

public interface UserRepository extends JpaRepository<User, Integer>{

}
