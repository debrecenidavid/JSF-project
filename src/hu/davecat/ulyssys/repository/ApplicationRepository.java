package hu.davecat.ulyssys.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import hu.davecat.ulyssys.entity.Application;

public interface ApplicationRepository extends JpaRepository<Application, Integer>{

}
