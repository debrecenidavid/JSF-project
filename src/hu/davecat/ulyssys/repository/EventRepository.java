package hu.davecat.ulyssys.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import hu.davecat.ulyssys.entity.Event;

public interface EventRepository extends JpaRepository<Event, Integer> {

}
