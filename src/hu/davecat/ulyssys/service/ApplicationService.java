package hu.davecat.ulyssys.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hu.davecat.ulyssys.entity.Application;
import hu.davecat.ulyssys.entity.Event;
import hu.davecat.ulyssys.repository.ApplicationRepository;

@Service
public class ApplicationService {

	@Autowired
	ApplicationRepository applicationRepository;
	
	public List<Application> findAll(){
		return applicationRepository.findAll();
	}
	
	public void approveApplicationById(int id){

		Application application= applicationRepository.findOne(id);
		application.setApproved(true);
		applicationRepository.save(application);
	}

	public void deleteApplicationById(int id) {
		applicationRepository.delete(id);

	}
	
	public void editApplicationAmountById(int id, int newamount){
		Application application= applicationRepository.findOne(id);
		application.setAmount(newamount);
		applicationRepository.save(application);
	}

	public void saveNewApplication(Application application) {
		applicationRepository.save(application);
	}
	
	public List<Application> findApproved(){
		List<Application> temp=applicationRepository.findAll();
		List<Application> result=new ArrayList<>();
		for (int i = 0; i < temp.size(); i++) {
			if (temp.get(i).isApproved()) {
				result.add(temp.get(i));
			}
		}
		return result;
	}
	
	public List<Application> findNotApproved(){
		List<Application> temp=applicationRepository.findAll();
		List<Application> result=new ArrayList<>();
		for (int i = 0; i < temp.size(); i++) {
			if (!temp.get(i).isApproved()) {
				result.add(temp.get(i));
			}
		}
		return result;
	}
	
	public List<Application> findApprovedByUserName(String loginname){
		List<Application> temp=applicationRepository.findAll();
		List<Application> result=new ArrayList<>();
		for (int i = 0; i < temp.size(); i++) {
			if (temp.get(i).isApproved()&&temp.get(i).getUser().getLoginname().equals(loginname)) {
				result.add(temp.get(i));
			}
		}
		return result;
	}
	
	public List<Application> findNotApprovedByUserName(String loginname){
		List<Application> temp=applicationRepository.findAll();
		List<Application> result=new ArrayList<>();
		for (int i = 0; i < temp.size(); i++) {
			if (!temp.get(i).isApproved()&&temp.get(i).getUser().getLoginname().equals(loginname)) {
				result.add(temp.get(i));
			}
		}
		return result;
	}

}
