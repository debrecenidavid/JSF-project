package hu.davecat.ulyssys.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hu.davecat.ulyssys.entity.User;
import hu.davecat.ulyssys.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	UserRepository userRepository;

	public User findUserByLoginName(String loginname) {
		List<User> temp = userRepository.findAll();
		int index = 0;
		while (!temp.get(index).getLoginname().equals(loginname)) {
			index++;
		}
		return temp.get(index);
	}

}
