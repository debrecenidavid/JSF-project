package hu.davecat.ulyssys.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hu.davecat.ulyssys.entity.Event;
import hu.davecat.ulyssys.repository.EventRepository;

@Service
public class EventService {

	@Autowired
	private EventRepository eventRepository;
	
	public List<Event> findAll(){
		return eventRepository.findAll();
	}
	
	public void newEvent(Event event){
		eventRepository.save(event);
	}

}
